<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeneficiariesEthinicitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiary_ethinicity', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('ethnicity_id')->unsigned();
            $table->bigInteger('beneficiary_id')->unsigned();

            $table->foreign('ethnicity_id')->references('id')->on('ethnicities')->onDelete('cascade');
            $table->foreign('beneficiary_id')->references('id')->on('beneficiaries')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiary_ethinicity');
    }
}
